// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAC_OYQKVEBlXHUJqYXqT8pw7ZLDj12WFU",
    authDomain: "lcmadmin.firebaseapp.com",
    projectId: "lcmadmin",
    storageBucket: "lcmadmin.appspot.com",
    messagingSenderId: "247130964811",
    appId: "1:247130964811:web:a72f92010825197ca15ece"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();


  //var codigo =0;
//var marca ="";
//var precio="";
//var descripcion ="";
//var seccion = "";
//var urlImag ="";
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("contenedorflexAIC");

    const dbref = refS(db, 'LACM');

    onValue(dbref, (snapshot) => {
        const productosPorSecciones = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const seccion = data.seccion;

            if (!productosPorSecciones[seccion]) {
                productosPorSecciones[seccion] = [];
            }
            productosPorSecciones[seccion].push(data);
        });

        section.innerHTML = '';

        for (const [seccion, productos] of Object.entries(productosPorSecciones)) {
            section.innerHTML += ` <section class="contenedor"><header id="seccion"><h1>${seccion}</h1> </header></section> `;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card' id='item'>
                   <img class='imagen id='urlImag' src='${producto.urlImag}' alt='' width='100%'>
                    <h2>${producto.marca}</h2>
                    <p>${producto.descripcion}</p>
                    <h4> ${producto.precio}</h4>

                    <button>Mas Detalles</button>
                </div>`;
            });
        }
    }, { onlyOnce: true });
}
