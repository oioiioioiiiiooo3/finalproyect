// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAC_OYQKVEBlXHUJqYXqT8pw7ZLDj12WFU",
    authDomain: "lcmadmin.firebaseapp.com",
    projectId: "lcmadmin",
    storageBucket: "lcmadmin.appspot.com",
    messagingSenderId: "247130964811",
    appId: "1:247130964811:web:a72f92010825197ca15ece"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();






  
var codigo =0;
var marca ="";
var precio="";
var descripcion ="";
var seccion = "";
var urlImag ="";

// variables para manejo de la imagen 

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');


function leerInputs(){
    codigo = document.getElementById('txtCodigo').value;
    marca = document.getElementById('txtMarca').value;
    precio =  document.getElementById('txtPrecio').value;
    descripcion = document.getElementById('txtDescripcion').value;
    seccion = document.getElementById('txtSeccion').value;
    urlImag = document.getElementById('txtUrl').value;
}
function mostarMensaje(mensaje){
    var mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent= mensaje;
    mensajeElement.style.display= 'block';
    setTimeout(()=>{
    mensajeElement.style.display ='none'},1000);
}
function limpiarInputs() {
    document.getElementById('txtCodigo').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtPrecio').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtSeccion').value = '';
    document.getElementById('txtUrl').value = '';
}
function escribirInputs() {
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtPrecio').value = precio;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtSeccion').value = seccion
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    let codigo = document.getElementById('txtCodigo').value.trim();
    if(codigo=== ""){
        mostarMensaje("Codigo no Ingresado");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'LACM/' + codigo)).then((snapshot) => {
        if(snapshot.exists()) {
            marca = snapshot.val().marca;
            precio = snapshot.val().precio;
            descripcion = snapshot.val().descripcion;
            seccion = snapshot.val().seccion;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostarMensaje("El Producto con Codigo" + codigo + "No existe.");
        }
    });
}

btnBuscar.addEventListener('click', buscarProducto);
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);
const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);
const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click',eliminarProducto);

function insertarProducto(){
    alert("ingrese a add db");
    leerInputs();

    if(codigo==="" || marca==="" || precio==="" || descripcion==="" || seccion==="" ){
        mostarMensaje("faltaron datos por capturar");
        return;
    }

    set(
         refS(db,'LACM/' + codigo),
         {
            codigo:codigo,
            marca:marca,
            precio:precio,
            descripcion:descripcion,
            seccion:seccion,
            urlImag:urlImag
         }
    ).then(()=>{

        alert("Se agrego con exito");
    }).catch((error)=>{
        alert("Ocurrio un error")
 });

 ListarProductos();
}

 ListarProductos()
 function ListarProductos() {
    const dbRef = refS(db, 'LACM');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
         const childKey = childSnapshot.key;

         const data = childSnapshot.val();
         var fila = document.createElement('tr');

         var celdaCodigo = document.createElement('td');
         celdaCodigo.textContent = childKey;
         fila.appendChild(celdaCodigo);

         var celdaMarca = document.createElement('td');
         celdaMarca.textContent = data.marca;
         fila.appendChild(celdaMarca);

         var celdaPrecio = document.createElement('td');
         celdaPrecio.textContent = data.precio;
         fila.appendChild(celdaPrecio);

         var celdaDescripcion = document.createElement('td');
         celdaDescripcion.textContent = data.descripcion;
         fila.appendChild(celdaDescripcion);

         var celdaSeccion = document.createElement('td');
         celdaSeccion.textContent = data.seccion;
         fila.appendChild(celdaSeccion);

         var celdaImagen = document.createElement('td');
         var imagen = document.createElement('img');
         imagen.src = data.urlImag;
         imagen.width = 100;
         celdaImagen.appendChild(imagen);
         fila.appendChild(celdaImagen);
         tbody.appendChild(fila);

    });
   },{onlyOnce: true});
 }


 function actualizarProducto () {
    leerInputs();
    if (codigo ==="" || marca ==="" || precio==="" || descripcion === "" || seccion ===""){
        mostarMensaje("Favor de capturar toda la informacion.");
        return;
    }
    alert("actualizar");
    update(refS(db, 'LACM/' + codigo), {
        codigo:codigo,
        marca:marca,
        precio:precio,
        descripcion:descripcion,
        seccion:seccion,
        urlImag:urlImag
    }).then(() => {
       mostarMensaje("Se actualizo con exito.");
       limpiarInputs();
       ListarProductos();
    }).catch((error) =>{
        mostarMensaje("Ocurrio un error: " + error);
    });
 }

 function eliminarProducto (){
    let codigo= document.getElementById('txtCodigo').value.trim();
    if (codigo === "") {
        mostarMensaje("No se ingreso un Codigo valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'LACM/' + codigo)). then((snapshot) => {

    if(snapshot.exists()){
        remove(refS(db, 'LACM/' + codigo))
        .then(() => {
            mostarMensaje("Producto eliminado con exito.");
            limpiarInputs();
            ListarProductos();
        })
        .catch((error) => {
            mostarMensaje("Ocurrio un error al eliminr el Producto:" + error);
        });
    } else {
        limpiarInputs();
        mostarMensaje("El Producto con Codigo" + codigo + "no existe.");
    }
    });
    ListarProductos();
}

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) => {
            const progress = (snapshot.bytesTrasnsferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
         }, (error) => {
            console.error(error);
         }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';
                }, 500);
                }).catch((error) => {
                    console.error(error);
                });
            });
         }
        });












